# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
#!/usr/bin/env python
from twisted.internet import gtk2reactor # for gtk-2.0
gtk2reactor.install()
import pygtk
pygtk.require('2.0')
import gtk
import adclib

class Gui():
    def delete_event(self, widget, event, data=None):
        return False

    def destroy(self, widget, data=None):
        print "destroy signal occurred"
        adclib.destroy()

    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self.window.connect("delete_event", self.delete_event)
    
        self.window.connect("destroy", self.destroy)
        
        #self.window.set_border_width(10)
        self.window.set_default_size(500, 500)
        
        self.notebook = gtk.Notebook()
        
        self.hbox = gtk.HBox()
        self.chatbox = gtk.VBox()
        self.textview = gtk.TextView()
        self.chatbox.add(self.textview)
        self.textview.set_editable(False)
        #self.hbox = gtk.HBox()
        self.entry = gtk.Entry()
        self.entry.connect("activate", self.sendmsg, None)
        #self.hbox.add(self.entry)
        self.chatbox.add(self.entry)
        #self.button = gtk.Button("Send")
        #self.button.connect("clicked", self.sendmsg, None)
        #self.hbox.add(self.button)
        self.chatbox.set_child_packing(self.entry, False, 0, 0, gtk.PACK_END)
        self.hbox.add(self.chatbox)
        
        self.treeview = gtk.TreeView()
        self.userlist = gtk.ListStore(str)
        self.treeview.set_model(self.userlist)
        self.tvcolumn = gtk.TreeViewColumn('Column 0')
        self.treeview.append_column(self.tvcolumn)
        self.cell = gtk.CellRendererText()
        self.tvcolumn.pack_start(self.cell, True)
        self.tvcolumn.add_attribute(self.cell, 'text', 0)
        self.treeview.set_search_column(0)

        self.hbox.add(self.treeview)
        self.notebook.append_page(self.hbox, gtk.Label("Chat"))
        
        self.window.add(self.notebook)

        self.window.show_all()
    
    def main(self):
        gtk.main()
        
    def sendmsg(self, widget, data=None):
        msg = self.entry.get_chars(0,-1)
        self.hc.sendMessage(msg)
        self.entry.delete_text(0,-1)

class HCGui(adclib.HCClient):
    def handleMessage(self, fromsid, text):
        self.factory.gui.textview.get_buffer().insert_at_cursor("<"+self.users[fromsid]['NI']+"> "+text+"\n")
        adclib.HCClient.handleMessage(self, fromsid, text)
        
    def connectionMade(self):
        self.factory.gui.hc = self
        adclib.HCClient.connectionMade(self)
        
    def handleUserInfo(self, sid, params):
        if "NI" in params:
            self.factory.gui.userlist.append([params["NI"]])
            pass
        adclib.HCClient.handleUserInfo(self, sid, params)

if __name__ == "__main__":
    gui = Gui()
    f = adclib.HCFactory()
    f.protocol = HCGui
    f.gui = gui
    adclib.reactor.connectTCP("localhost", 8359, f)
    adclib.reactor.run()