# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
#!/usr/bin/env python
import tiger
import random
import binascii
import base64
from twisted.internet import reactor, protocol, ssl
from OpenSSL import SSL
import lxml.etree as ET
from xml.sax.saxutils import quoteattr
import os
import bz2
import mmap
import pickle
import config

## TODO
# Better randomness ?
# Handle unicode searches properly

def leafhash(x):
    return tiger.hash('\x00'+x)
    
def internalhash(x):
    return tiger.hash('\x01'+x)

def previt(leaf, prev, level):
    if level in prev:
        prev = previt(internalhash(prev[level]+leaf), prev, level+1)
        del(prev[level])
    else:
        prev[level] = leaf
    return prev

def hashtest(buf):
    leaves = ''
    level = 0
    prev = {}
    while True:
        chunk = buf.read(1024)
        if chunk == '':
            break
        leaf = leafhash(chunk)
        prev = previt(leaf, prev, level)
    nh = ''
    for h in prev.values():
        if (nh == ''):
            nh = h
        else:
            nh = internalhash(h+nh)
    return nh

def tigerleaves(buf):
    out = ''
    while True:
        chunk = buf.read(1024)
        if chunk == '':
            break
        out += leafhash(chunk)
    return out

def longToString(n):
    return binascii.unhexlify("%x" % n)

class FileShare:
    cid = None
    fileList = None
    hashes = {}
    tree = None
    
    def __init__(self, cid):
        self.cid = cid
        self.createFileList()
    
    def dirIter(self, dir, parent):
        files=os.listdir(dir)
        for file in files:
            if os.path.isdir(dir+"/"+file):
                f = ET.SubElement(parent, "Directory")
                f.set("Name", file)
                self.dirIter(dir+"/"+file, f)
            else:
                f = ET.SubElement(parent, "File")
                f.set("Name", file)
                f.set("Size", str(os.path.getsize(dir+"/"+file)))
                # FIXME Passing as string will fail for large files
                fp = open(dir+"/"+file, 'r')
                tth = base64.b32encode(tiger.treehash(mmap.mmap(fp.fileno(), 0, access=mmap.ACCESS_READ))).strip('=')
                print tth
                #print base64.b32encode(hashtest(mmap.mmap(fp.fileno(), 0, access=mmap.ACCESS_READ)))
                f.set("TTH", tth)
                self.hashes[tth.strip('=')] = dir+"/"+file
    
    def createFileList(self):
        root = ET.Element("FileListing")
        root.set("Version", "1")
        root.set ("CID", self.cid)
        root.set("Base", "/")
        self.dirIter(config.shareDir, root)
        self.tree = ET.ElementTree(root)
        self.fileList = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + ET.tostring(root)
        
    def getFileList(self):
        return self.fileList
    
    def getBZ2FileList(self):
        return bz2.compress(self.getFileList())
        
    def search(self, params):
        if "TR" in params:
            results = self.tree.xpath("//File[@TTH='"+params["TR"]+"']")
            return results
        pred = "1=1"
        if "AN" in params:
            if type(params["AN"]).__name__ == 'str':
                params["AN"] = [params["AN"]]
            for term in params["AN"]:
                pred += " and contains(@Name, "+quoteattr(term)+")"
                #print pred
        if "EX" in params:
            if type(params["EX"]).__name__ == 'str':
                params["EX"] = [params["EX"]]
            for ext in params["EX"]:
                ext = "."+ext
                pred += " and '"+ext+"'=substring(@Name,string-length(@Name)-"+str(len(ext))+"+1)"
        if "NO" in params:
            if type(params["NO"]).__name__ == 'str':
                params["NO"] = [params["NO"]]
            for term in params["NO"]:
                pred += "and not(contains(@Name, '"+quoteattr(term)+"'))"
        #results = self.tree.xpath("//File[contains(@Name,'raven') and substring(@Name,2,3)='ave']")
        results = []
        if pred != "1=1":
            results = self.tree.xpath(u"//File["+pred.decode("utf-8")+u"]")
        return results

class DCClient(protocol.Protocol):
    
    def sendLine(self, text):
        self.transport.write(text+"\n")
    
    def connectionLost(self, reason):
        print "Connection lost"
        
    def dataReceived(self, data):
        lines = data.split('\n')
        for line in lines:
            self.handleLine(line)
            
    def namedParameters(self, words):
        parameters = {}
        for word in words:
            if word[:2] in parameters:
                if type(parameters[word[:2]]).__name__ == 'str':
                    parameters[word[:2]] = [parameters[word[:2]], word[2:]]
                else:
                    parameters[word[:2]].append(word[2:])
            else:
                parameters[word[:2]] = word[2:]
        return parameters

class HCClient(DCClient):
    nick = config.nick
    id = None
    pd = None
    sid = None
    fs = None
    tlssever = None
    users = {}
    
    def sendLine(self, text):
        DCClient.sendLine(self, text)
        print "<<< "+text
        
    def sendMessage(self, text):
        self.sendLine("BMSG "+self.sid+" "+self.escape(text));
    
    def sendPrivateMessage(self, tosid, text):
        self.sendLine("EMSG "+self.sid+" "+tosid+" "+self.escape(text)+" PM"+self.sid);
    
    def connectionMade(self):
        self.sendLine("HSUP ADBASE ADTIGR")
        self.tlsserver = CCServerFactory()
        reactor.listenSSL(config.tlsPort, self.tlsserver, ssl.DefaultOpenSSLContextFactory(os.path.join(config.keyDir,"server.key"), os.path.join(config.keyDir,"server.crt"), SSL.TLSv1_METHOD))

    
    def handleLine(self, line):
        words = line.split(' ')
        
        if words[0] != "BINF":
            print ">>> "+line
        
        if words[0] == "ISID":
            self.sid = words[1]
            self.inf()
        elif words[0] == "BINF":
            sid = words[1]
            self.handleUserInfo(sid, self.namedParameters(words[2:]))
        elif words[0] == "IQUI":
            sid = words[1]
            self.handleUserQuit(sid)
        elif words[0] == "DCTM":
            self.connectTo(words[1], words[3], words[4], words[5])
        elif words[0] == "BMSG":
            self.handleMessage(words[1], self.unescape(words[2]))
        elif words[0] == "DMSG" or words[0] == "EMSG":
            if len(words) > 3:
                self.handlePrivateMessage(words[1], self.unescape(words[3]))
        elif words[0] == "BSCH" or words[0] == "FSCH":
            self.handleSearch(words[1], self.namedParameters(words[2:]))
        elif words[0] == "DRES":
            self.handleResult(words[1], self.namedParameters(words[2:]))
            
    def handleUserInfo(self, sid, params):
        self.users[sid] = params
        
    def handleUserQuit(self, sid):
        pass
            
    def handleMessage(self, fromsid, text):
        pass
    
    def handlePrivateMessage(self, fromsid, text):
        pass
    
    def inf(self):
        if self.id == None:
            r = random.getrandbits(199) + (1<<199)
            if not "ids.pickle" in os.listdir("."):
                rstring = longToString(r)
                self.pd = base64.b32encode(rstring).strip('=')
                hstring = tiger.hash(rstring)
                self.id = base64.b32encode(hstring).strip('=')
                pickle.dump({"pd":self.pd, "id":self.id}, open("ids.pickle", "w"))
            else:
                ids = pickle.load(open("ids.pickle", "r"))
                self.pd = ids["pd"]
                self.id = ids["id"]
            self.fs = FileShare(self.id)
            self.tlsserver.id = self.id
        self.sendLine("BINF "+self.sid+" ID"+self.id+" PD"+self.pd+" NI"+self.nick+" I40.0.0.0"+"U48360 SUADC0,TCP4,UDP4 SS"+str(101*1024*1024))
    
    def connectTo(self, sid, protocol, port, token):
        print protocol+" "+port
        ip = self.users[sid]['I4']
        cf = CCFactory()
        cf.hc = self
        cf.token = token
        ccf = ssl.ClientContextFactory()
        ccf.method = SSL.TLSv1_METHOD 
        reactor.connectSSL(ip, int(port), cf, ccf)
        
    def escape(self, text):
        for chars in [ ('\\', '\\\\'), (' ','\\s'), ('\n', '\\n') ]:
            text = text.replace(chars[0], chars[1])
        return text
    
    def unescape(self, text):
        for chars in [ ('\\', '\\\\'), (' ','\\s'), ('\n', '\\n') ]:
            text = text.replace(chars[1], chars[0])
        return text
    
    def getFile(self, tosid, file, location='', downloader=None):
        if location == '':
            location = file
        token = str(random.randint(0,10000000000))
        self.tlsserver.sessions[token] = {"token":token, "user":self.users[tosid], "file":file, "location":location, "downloader":downloader}
        self.sendLine("DCTM "+self.sid+" "+tosid+" ADCS/0.10 "+str(config.tlsPort)+" "+token)
    
    def handleSearch(self, user, params):
        for result in self.fs.search(params):
            self.sendLine("DRES "+self.sid+" "+user+" TO"+params["TO"]+" FN"+self.xmlfilename(result)+" SI"+result.get("Size")+" TR"+result.get("TTH")+" SL1")
    
    def handleResult(self, user, params):
        pass
    
    def xmlfilename(self, elem):
        if elem.getparent().tag == "Directory":
            parentfilename = self.xmlfilename(elem.getparent())
        else: parentfilename = ""
        filename = parentfilename+"/"+elem.get("Name")
        return filename
    
    def doSearch(self, query):
        self.sendLine("BSCH "+self.sid+" "+query+" TOauto")
        
    
class HCFactory(protocol.ClientFactory):
    protocol = HCClient
    def clientConnectionFailed(self, connector, reason):
        print "Connection failed - goodbye!"
        #reactor.stop()
    def clientConnectionLost(self, connector, reason):
        print "Connection lost - goodbye!"
        #reactor.stop()

class CCClient(DCClient):
    def connectionMade(self):
        self.sendLine("CSUP ADBASE ADTIGR")

    def sendLine(self, text):
        DCClient.sendLine(self, text)
        print "C<<< "+text

    def handleLine(self, line):
        print "C>>> "+line
        words = line.split(' ')
        if words[0] == "CSUP":
            self.sendLine("CINF ID"+self.factory.hc.id+" TO"+self.factory.token)
        elif words[0] == "CGET":
            fs = self.factory.hc.fs
            if words[2] == "files.xml":
                out = selffs.getFileList()
                self.sendLine("CSND "+words[1]+" "+words[2]+" 0 "+str(len(out)))
                self.transport.write(out)
            elif words[2] == "files.xml.bz2":
                out = fs.getBZ2FileList()
                self.sendLine("CSND "+words[1]+" "+words[2]+" 0 "+str(len(out)))
                self.transport.write(out)
            elif words[2][:4] == "TTH/":
                filename = fs.hashes[words[2][4:].strip('=')]
                fp = open(filename, 'r')
                offset = int(words[3])
                length = int(words[4])
                if length == -1:    length = 0
                extra = offset%mmap.ALLOCATIONGRANULARITY
                if (length == 0):   l = 0
                else:   l = extra + length
                mm = mmap.mmap(fp.fileno(), l, access=mmap.ACCESS_READ, offset=(offset//mmap.ALLOCATIONGRANULARITY)*mmap.ALLOCATIONGRANULARITY)
                mm.read(extra)
                if words[1] == "file":
                    if length == 0:
                        length = mm.size()
                    out = mm.read(length)
                elif words[1] == "tthl":
                    out = tigerleaves(mm)
                    length = len(out)
                else:
                    return
                self.sendLine("CSND "+words[1]+" "+words[2]+" "+words[3]+" "+str(length))
                self.transport.write(out)
            else:
                self.sendLine("CSTA 151 File\snot\sfound")

class CCFactory(protocol.ClientFactory):
    protocol = CCClient
    def clientConnectionFailed(self, connector, reason):
        print "Connection failed"
    def clientConnectionLost(self, connector, reason):
        print "Connection closed"

class CCServer(DCClient):
    text = True
    file = None
    token = None
    sentBytes = 0
    fileSize = 0
    def connectionMade(self):
        pass
        
    def connectionLost(self, reason):
        print "(Server) Connection lost"
        
    def sendLine(self, text):
        DCClient.sendLine(self, text)
        print "CS<<< "+text    
    
    def dataReceived(self, data):
        if self.text:
            DCClient.dataReceived(self,data)
        else:
            self.file.write(data)
            self.sentBytes += len(data)
            if self.sentBytes >= self.fileSize:
                self.file.close()
                self.transport.loseConnection()            
        
    def handleLine(self, line):
        print "CS>>> "+line
        words = line.split(' ')
        if words[0] == "CSUP":
            self.sendLine("CSUP ADBASE ADTIGR")
            self.sendLine("CINF ID"+self.factory.id)
        elif words[0] == "CINF":
            params = self.namedParameters(words[1:])
            self.token = params['TO']
            if params['ID'].strip('=') == self.factory.sessions[self.token]['user']['ID'].strip('='):
                file = self.factory.sessions[self.token]['file']
                self.sendLine("CGET file "+file+" 0 -1")
            else:
                self.transport.loseConnection()
        elif words[0] == "CSND":
            self.text = False
            self.file = open(os.path.join(self.factory.sessions[self.token]['location']),"w")
            self.fileSize = int(words[4])
        elif words[0] == "CSTA":
            if words[1] == "153":
                self.factory.sessions[self.token]['downloader'].try_next()
    
    def connectionLost(self, reason):
        del self.factory.sessions[self.token]

class CCServerFactory(protocol.ServerFactory):
    sessions = {}
    protocol = CCServer

# this connects the protocol to a server runing on port 8000
def main():
    f = HCFactory()
    reactor.connectTCP("localhost", config.dtellaPort, f)
    reactor.run()

def destroy():
    reactor.stop()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
