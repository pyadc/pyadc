import adclib
import os
import pickle
import datetime
import config

class Downloader:
    sources = []
    client = None
    
    def __init__(self, client):
        self.client = client
    
    def add_source(self, params):
        self.sources.append(params)
        print params
        if len(self.sources) < 2:
            self.download(params)
    
    def try_next(self):
        self.sources.pop()
        if len(self.sources):
            self.download(self.sources[0])
    
    def download(self, params):
        short = self.client.unescape(params["FN"]).split("/")[-1]
        self.client.getFile(params["sid"], params["FN"], "downloads/"+short, self)
    
class BotClient(adclib.HCClient):
    def handleMessage(self, fromsid, text):
        #if self.users[fromsid]['NI'] == "PissChrist":
        #    self.sendMessage("No, you!")
        pass
    
    def handlePrivateMessage(self, fromsid, text):
        if text == "!die":
            import sys
            sys.exit(0)
        elif text == "!connect":
            self.getFile(fromsid, "files.xml", "test")
        elif text == "!search":
            self.doSearch("ANraven")
        #elif text == "!spider":
        #    for (sid, user) in self.users.items():
        #        if "NI" in user:
        #            print sid, user["NI"]
        #            self.getFile(sid, "files.xml", "lists/"+user["NI"]+".xml")
        elif text == "!count":
            from xml.etree.ElementTree import iterparse, tostring
            for file in os.listdir("lists"):
                f = open("lists/"+file, "r")
                for tag in iterparse(f, events=['start']):
                    print tag
        elif text.split(" ")[0] == "!download":
            if len(text.split(" ")) > 1:
                self.doSearch("TR" + text.split(" ")[1])
                self.downloader = Downloader(self)
                #self.doDownload = True
        else:
            self.sendPrivateMessage(fromsid, "Command not understood.")
    
    def handleUserInfo(self, sid, params):
        adclib.HCClient.handleUserInfo(self, sid, params)
        if config.girtonWarning and "LO" in params and params["LO"] == "Girton":
            gusers = pickle.load(open(os.path.join(self.factory.dir, "girtonusers.pickle"),"r"))
            if not params["NI"] in gusers:
                self.sendPrivateMessage(sid, "Hi, you seem to be connecting from Girton. You need to change the settings so that all ports are below 1024. More information can be found at http://wiki.camdc.net/act/lib/girton or by asking in the chat.")
                self.sendPrivateMessage(sid, "This is an automated message and should display only once. For more information contact PissChrist or thirteenthdoctor.")
                gusers[params["NI"]] = params
            gusers = pickle.dump(gusers, open(os.path.join(self.factory.dir, "girtonusers.pickle"),"w"))
        
    def handleSearch(self, user, params):
        if "AN" in params:
            f = open("search.log", "a")
            if params["AN"].__class__.__name__ == "str": queries = [params["AN"]]
            else: queries = params["AN"]
            f.write(str(datetime.datetime.now()) + " " + self.users[user]["NI"] + " " + " ".join(queries) + "\n")
        adclib.HCClient.handleSearch(self, user, params)

    def handleResult(self, user, params):
        params["sid"] = user
        self.downloader.add_source(params)

class BotFactory(adclib.HCFactory):
    protocol = BotClient
    dir = "."
    def __init__(self):
        if config.girtonWarning:
            if not "girtonusers.pickle" in os.listdir(self.dir):
                pickle.dump({}, open(os.path.join(self.dir, "girtonusers.pickle"), "w"))
            if not "downloads" in os.listdir(self.dir):
                os.mkdir(os.path.join(self.dir, "downloads"))
        if not "lists" in os.listdir(self.dir):
            os.mkdir(os.path.join(self.dir, "lists"))

if __name__ == '__main__':
    from twisted.internet import reactor
    f = BotFactory()
    reactor.connectTCP("localhost", config.dtellaPort, f)
    reactor.run()
