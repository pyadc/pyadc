import os

total = 0
tths = {}

from xml.etree.ElementTree import iterparse, tostring
for file in os.listdir("lists"):
    f = open("lists/"+file, "r")
    for (event, el) in iterparse(f, events=['start']):
        if el.tag == "File":
            #print el.get("Name")
            tth = el.get("TTH")
            if True or not tth in tths:
                total += int( el.get("Size") )
                tths[tth] = 0
    print total, "(", total/1024/1024/1024/1024, "TB", ")"