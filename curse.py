# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
#!/usr/bin/env python
import curses

stdscr = curses.initscr()
curses.start_color()
curses.noecho()
curses.cbreak()
stdscr.keypad(1)

begin_x = 20 ; begin_y = 7
height = 5 ; width = 40
win = curses.newwin(height, width, begin_y, begin_x)

curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
win.addstr( "Pretty text", curses.color_pair(1) )
win.refresh()

while (True):
    c = stdscr.getch()
    if c == ord('q'): break  # Exit the while()


curses.nocbreak(); stdscr.keypad(0); curses.echo()
curses.endwin()