import adclib
from twisted.internet import reactor
    
class BotClient(adclib.HCClient):
    nick = "spam"
    
    def handleSearch(self, user, params):
        if "TR" in params:
            adclib.HCClient.handleSearch(self, user, params)
        
        cheat = ""
        if "AN" in params:
            if type(params["AN"]).__name__ == 'str':
                params["AN"] = [params["AN"]]
            for term in params["AN"]:
                cheat += "/"+self.escape(term)
            message = "Spam, lovely Spam, wonderful Spam"
            faketiger = "3TWK6Y5NKIP6TOLPOI43R5NJLZRSBE2VLQAXIEI" #"5SNJEU5CWCRIOC3Z4IEUWSGQS4TSSAQVZDQY6NI"
            ext = ".jpg"
            size = "391124" #"60942"
            self.sendLine("DRES "+self.sid+" "+user+" TO"+params["TO"]+" FN"+cheat+"/"+self.escape(message)+"/"+self.escape(message)+ext+" SI"+size+" TR"+faketiger+" SL1")

class BotFactory(adclib.HCFactory):
    protocol = BotClient

f = BotFactory()
reactor.connectTCP("localhost", 8359, f)
reactor.run()
